import { add, multiply, subtract } from "./main";
import { expect } from "@esm-bundle/chai";

describe("main tests", () => {
  it("subtracts two numbers", () => {
    expect(subtract(3, 5)).to.equal(-2);
  });

  it("adds two numbers", () => {
    expect(add(3, 5)).to.equal(8);
  });

  it("subtracts two number", () => {
    expect(subtract(5, 3)).to.equal(2);
  });

  it("multiplies two number", () => {
    expect(multiply(5, 3)).to.equal(15);
  });
});
